# bilibili-style

#### 介绍
哔哩哔哩半透明主题

#### 安装教程

1. 在谷歌插件商店 安装Stylus插件，直达链接：[Stylus-Chrome应用商店](https://chrome.google.com/webstore/detail/stylus/clngdbkpkpeebahjckkjfobafhncgmne)
2. UserStylesWorld主题样式社区直达链接：[哔哩哔哩半透明主题](https://userstyles.world/style/7728/default-slug)
3. 点击Install安装样式脚本
4. 可自行选择是否检查更新等
5. 进入B站可见主题样式呈现，若无刷新重试

#### 使用说明

1. 当出现排版混乱等问题，可左键Stylus插件，将主题样式取消选中，B站会恢复为原样式

#### 效果展示

![半透明背景主页展示效果](%E5%8D%8A%E9%80%8F%E6%98%8E%E8%83%8C%E6%99%AF%E4%B8%BB%E9%A1%B5%E5%B1%95%E7%A4%BA.jpg)