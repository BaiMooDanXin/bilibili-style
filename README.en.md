# bilibili-style

#### Description
哔哩哔哩半透明主题

#### Software Architecture
Software architecture description

#### Installation

1. Install the [Stylus] plugin in the Google Plugin or edge Plugin store
2. Style community UserStylesWorld direct link：[UserStylesWorld](https://userstyles.world/style/7728/default-slug)
3. Click Install to Install the style script
4. Can choose whether to check for updates and so on
5. Enter bilibili to see the theme style render, if no refresh try again

#### Instructions

1. When there are problems such as typographical confusion, you can use the Stylus plug-in to uncheck the theme style, and bilibili will return to its original style

#### Effect
![半透明背景主页展示效果](%E5%8D%8A%E9%80%8F%E6%98%8E%E8%83%8C%E6%99%AF%E4%B8%BB%E9%A1%B5%E5%B1%95%E7%A4%BA.jpg)